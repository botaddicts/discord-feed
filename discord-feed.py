# Every 10 minutes check for al feed urls
import json, feedparser, time
from discord_webhook import DiscordWebhook
from config import webhook_url, delay

while True:
    urls = json.loads(open("urls.json").read())
    for url in urls:
        try:
            d = feedparser.parse(url)
            latest = d.entries[0]
            try:
                time.struct_time(urls[url])
            except:
                print("Invalid time found")
                urls[url] = json.loads(json.dumps(latest.updated_parsed))

            if latest.updated_parsed > time.struct_time(urls[url]):
                urls[url] = latest.updated_parsed
                print(f"{latest.link} | {latest.title}")
                DiscordWebhook(url=webhook_url, content=f":bell: | **{d.feed.title}** | {latest.title}").execute()
                DiscordWebhook(url=webhook_url, content=latest.link).execute()
            else:
                print(f"No new entry in {d.feed.title}")

        except:
            print(f"{url} is an invalid link")

    open("urls.json", "w").write(json.dumps(urls, indent=4))
    time.sleep(delay)
