# Discord-Feed
**Discord-Feed** is a Python script that relays new entries in several RSS feeds through Discord webhooks feature. It doesn't require a bot to be used.

This means this script can be used to follow blogs, podcasts, news websites, YouTube channels (or others), editions of Wikipedia pages, and many others.

## What can you follow?
RSS feed is an old technology but is still very useful and used today. This is a short list of stuff you can follow using this script and using RSS feeds:

* Blogs
* Podcasts
* News websites
* YouTube channels (and other platforms as well)
* Modifications of pages on a wiki (i.e Wikipedia)
* New entries on a forum/link aggreator (i.e Reddit)

## How to install this script
1. Install the tools

```bash
sudo apt install wget git python3-pip
```

2. Download the source code

```bash
git clone https://codeberg.org/SnowCode/discord-feed
cd discord-feed/
```

3. Install the Python modules

```bash
pip3 install -r requirements.txt
```

4. Make a file called `config.py` with the following content:

```python
webhook_url = "your webhook url goes here"
delay = 600
```

5. Make a file called `urls.json` where you'll add all the RSS feed urls under the following template:

```json
{
    "link1": "",
    "link2": "",
    "link3": ""
}
```

6. Run the script to see if it works. If it does, press CTRL+C and move to the next step:

```bash
python3 discord-feed.py
```

7. Add the service file and launch systemd

```bash
sed -i "s/ABSOLUTE_PATH/$(pwd)/g" discord-feed.service
sed -i "s/USER/$(whoami)/g" discord-feed.service
sudo cp discord-feed.service /etc/systemd/system/discord-feed.service

sudo systemctl start discord-feed.service
```


